# React Application

This basic React application uses Webpack to compile JavaScript modules. Since React uses JSX (non JavaScript standard), Babel is used to transpile the JSX.

## Features

- Ability to use Webpack and Babel in React App.
- Use Webpack to transpile styles (.css, .sass/scss, .less)

## References

- [jakoblind](https://createapp.dev/webpack)
- [webpack.js.org](https://webpack.js.org/guides/getting-started/)
- [webpack.js.org](https://webpack.js.org/loaders/less-loader/)
- [webpack.js.org](https://webpack.js.org/loaders/sass-loader/)
