import React from "react";
import ReactDOM from "react-dom";

require("./styles.css");
require("./styles.less");
require("./styles.scss");

class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Using Css</h1>
        <h2>Using Less</h2>
        <p>and this also for Less</p>
        <h3>Using Scss / Sass</h3>
        Hi {this.props.name}{" "}
      </div>
    );
  }
}

var mountNode = document.getElementById("app");
ReactDOM.render(<App name="world"> </App>, mountNode);
